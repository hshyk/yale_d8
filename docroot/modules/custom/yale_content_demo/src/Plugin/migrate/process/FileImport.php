<?php

namespace Drupal\yale_content_demo\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Migration process plugin for migrating an image.
 *
 * @MigrateProcessPlugin(
 *   id = "file_import"
 * )
 */
class FileImport extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = drupal_get_path('module', 'yale_content_demo') . '/data/images/' . $value;

    if (!$uri = file_unmanaged_copy($source)) {
      return [];
    }

    $file = \Drupal::entityTypeManager()->getStorage('file')->create(['uri' => $uri]);
    $file->save();

    return $file->id();
  }

}
