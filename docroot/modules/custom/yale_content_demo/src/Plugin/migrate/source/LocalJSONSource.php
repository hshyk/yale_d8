<?php

namespace Drupal\yale_content_demo\Plugin\migrate\source;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_source_json\Plugin\migrate\source\JSONSource;

/**
 * A source class for JSON files.
 *
 * @MigrateSource(
 *   id = "local_json_source"
 * )
 */
class LocalJSONSource extends JSONSource {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, array $namespaces = []) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $config_fields = [
      'headers',
      'fields',
      'identifier',
    ];

    // Store the configuration data.
    foreach ($config_fields as $config_field) {
      if (isset($configuration[$config_field])) {
        $this->{$config_field} = $configuration[$config_field];
      }
      else {
        // Throw Exception.
        throw new MigrateException('The source configuration must include ' . $config_field . '.');
      }
    }

    $data_file = $this->path;
    $data_path = drupal_get_path('module', 'yale_content_demo') . '/data/' . $data_file;
    $this->path = file_create_url($data_path);

    // Allow custom reader and client classes to be passed as config settings.
    $this->clientClass = !isset($configuration['clientClass']) ? '\Drupal\migrate_source_json\Plugin\migrate\JSONClient' : $configuration['clientClass'];
    $this->readerClass = !isset($configuration['readerClass']) ? '\Drupal\migrate_source_json\Plugin\migrate\JSONReader' : $configuration['readerClass'];

    // Create the JSON reader that will process the request, pass it config.
    $this->reader = new $this->readerClass($configuration);
  }

}
